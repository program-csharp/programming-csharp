﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Лабораторна__23
{
    internal class WorkWithTxtFiles
    {
        //Read information
        public static string readStringFromFile(string fileName)
        {
            return File.ReadAllText(fileName);
        }
        public static double[] readArrayFromString(string fileName)
        {
            double[] numbers;
            string data = readStringFromFile(fileName);
            char[] separators = new char[] { ' ', ',' };
            string[] parts = data.Split(separators, StringSplitOptions.RemoveEmptyEntries);
            numbers = new double[parts.Length];
            for (int i = 0; i < parts.Length; i++)
            {
                numbers[i] = Convert.ToDouble(parts[i].Replace(".",","));
            }
            return numbers;
        }

        //Write information
        public static string writeArrayToString(double[] data)
        {
            string message = "";
            message += Convert.ToString(data[0]);
            for (int i =1; i < data.Length; i++)
            {
                message += ", "+Convert.ToString(data[i]);
            }
            return message;
        }
        public static void writeStringToFile(string fileName, double[] data)
        {
            string message = writeArrayToString(data);
            StreamWriter SW = new StreamWriter(fileName);
            SW.WriteLine(message);
            SW.Close();
        }
    }
}
