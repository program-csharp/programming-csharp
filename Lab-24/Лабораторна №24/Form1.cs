﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;
using System.Xml.Schema;

namespace Лабораторна__24
{
    public partial class Form1 : Form
    {
        Random rand = new Random(DateTime.Now.Millisecond);
        List<PictureBox> pictureItems = new List<PictureBox>();
        int scores = 0;
        public Form1()
        {
            InitializeComponent();
        }

        private void startToolStripMenuItem_Click(object sender, EventArgs e)
        {
            timer1.Enabled = true;
        }

        private void rulesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Quadrats appear on the screen to be clicked on. If there are more than 10 quadrats on the screen, you lose.");
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void restartToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach(var elem in pictureItems)
                this.Controls.Remove(elem);
            scores = 0;
            pictureItems.Clear();
            startToolStripMenuItem_Click(sender, e);
        }

        private void MakeElement()
        {
            //Create element
            var element = new PictureBox();
            //Create coordinats (x,y)
            int x = rand.Next(50, this.ClientSize.Width-element.Width);
            int y = rand.Next(50, this.ClientSize.Height-element.Height);
            //Create quadrat
            element.Height = 35;
            element.Width = 35;
            element.BackColor = Color.Red;
            element.Location = new Point(x, y);
            element.Click += Element_Click;
            //Add quadrat to List and Form
            pictureItems.Add(element);
            this.Controls.Add(element);
        }

        private void Element_Click(object sender, EventArgs e)
        {
            PictureBox element = sender as PictureBox;
            pictureItems.Remove(element);
            this.Controls.Remove(element);
            timer1.Interval -= 2;
            label2.Text = "Scores " + ++scores; //++ стоїть, щоб спочатку змінилося значення scores, а вже потім вивелося
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            if (pictureItems.Count<10)
            {
                MakeElement();
                label1.Text = "Counter quadrats: " + pictureItems.Count().ToString();
            }
            else
            {
                timer1.Stop();
                timer1.Enabled = false;
                MessageBox.Show("You lost");
            }
        }
    }
}
