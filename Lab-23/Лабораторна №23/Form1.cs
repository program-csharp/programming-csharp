﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Net.Mime.MediaTypeNames;

namespace Лабораторна__23
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            dataGridView1.RowCount = 1;
            dataGridView1.ColumnCount = 1;
        }

        private void відкритиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "txt files (*.txt)|*.txt|dat files (*.dat)|*.dat";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                if (openFileDialog1.FilterIndex == 1)
                {
                    WorkWithDataGridView.WriteToGrid(dataGridView1, WorkWithTxtFiles.readArrayFromString(openFileDialog1.FileName));
                    numericUpDown1.Value = dataGridView1.ColumnCount;
                }
                else if (openFileDialog1.FilterIndex == 2)
                {
                    WorkWithDataGridView.WriteToGrid(dataGridView1, WorkWithDatFiles.readArrayFromFile(openFileDialog1.FileName));
                    numericUpDown1.Value = dataGridView1.ColumnCount;
                }
                else
                {
                    MessageBox.Show("Not Work");
                }
            }
        }

        private void зберегтиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Filter = "txt files (*.txt)|*.txt|dat files (*.dat)|*.dat";
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                if (saveFileDialog1.FilterIndex == 1)
                {
                    double[] d = WorkWithDataGridView.ReadFromGrid(dataGridView1);
                    WorkWithTxtFiles.writeStringToFile(saveFileDialog1.FileName, d);
                }
                else if(saveFileDialog1.FilterIndex == 2)
                {
                    double[] d = WorkWithDataGridView.ReadFromGrid(dataGridView1);
                    WorkWithDatFiles.writeArrayToFile(saveFileDialog1.FileName, d);
                }
                else
                {
                    MessageBox.Show("Not Work");
                }
            }
        }

        private void вихідToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            dataGridView1.ColumnCount = Convert.ToInt32(numericUpDown1.Value);
        }

        private void найбільшийЕлементToolStripMenuItem_Click(object sender, EventArgs e)
        {
            double[]n = WorkWithDataGridView.ReadFromGrid(dataGridView1);
            double max = -1000;
            foreach(var item in n)
            {
                if (item < 0 && item>max)
                {
                    max = item;
                }
            }
            MessageBox.Show(max.ToString()); 
        }
    }
}
