﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Лабораторна__23
{
    internal class WorkWithDataGridView
    {
        public static double [] ReadFromGrid(DataGridView grid)
        {
            double[] data = new double[grid.ColumnCount];
            for(int i = 0; i < grid.ColumnCount; i++)
            {
                data[i] = Convert.ToDouble(grid[i, 0].Value);
            }
            return data;
        }
        public static void WriteToGrid(DataGridView grid, double[] data)
        {
            grid.ColumnCount = data.Length;
            for(int i = 0; i < data.Length; i++)
            {
                grid[i, 0].Value = data[i];
            }
        }
    }
}
