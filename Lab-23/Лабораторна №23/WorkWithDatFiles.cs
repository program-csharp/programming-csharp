﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace Лабораторна__23
{
    [Serializable]
    internal class WorkWithDatFiles
    {
        //Read information
        public static double[] readArrayFromFile(string fileName)
        {
            FileStream FS = new FileStream(fileName, FileMode.Open);
            BinaryFormatter BF = new BinaryFormatter();
            double[] a = (double[])BF.Deserialize(FS);
            FS.Close();
            return a;
        }

        //Write information
        public static void writeArrayToFile(string fileName, double[] data)
        {
            FileStream FS = new FileStream(fileName, FileMode.Create);
            BinaryFormatter BF = new BinaryFormatter();
            BF.Serialize(FS, data);
            FS.Close();
        }
    }
}
